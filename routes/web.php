<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('getUserInfo', 'UserController@userInfo');
Route::get('userList', 'UserController@lists');

Route::get('userSubmit', 'UserController@submit');
Route::get('user/profile', 'UserProfileController@show')->name('profile');
Route::get('user/export', 'UserController@export');


Route::group(['prefix' => 'test', 'namespace' => ''], function () {

    Route::get('cache', 'TestController@export');


});

Route::group(['prefix' => 'cache'], function () {

    Route::get('set', 'CacheController@set');

    Route::get('get', 'CacheController@get');

    Route::get('redis_publish', 'CacheController@redisPublish');

});


Route::group(['prefix' => 'order'], function () {

    Route::get('on_put_order', 'OrderController@onPutOrder');


});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
