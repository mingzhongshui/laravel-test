<?php

namespace App\Jobs\Test\Order;

use App\Model\Test\Order\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class AutoCloseQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $orderId;

    /**
     * Create a new job instance.
     *
     * @param $orderId
     * @return void
     */
    public function __construct($orderId)
    {

        $this->orderId = $orderId;
        logger('__construct --- ' . $orderId);
    }


    public function handle()
    {
        try {

            Order::where('id', $this->orderId)->update(['status' => -1]);

        } catch (\Exception $e) {

            logger('AutoCloseQueue error ---orderId:' . $this->orderId);
            logger($e->getMessage() . '------' . $e->getCode());
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }
}
