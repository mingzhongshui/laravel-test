<?php

namespace App\Model\Test\Order;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    public $table = 'order';


    public $timestamps = true;


    const CREATED_AT = 'create_at';
    const UPDATED_AT = 'update_at';


    protected function asDateTime($value)
    {
        return $value;
    }

    public function fromDateTime($value)
    {
        return $value;
    }
}
