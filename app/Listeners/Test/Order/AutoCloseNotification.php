<?php

namespace App\Listeners\Test\Order;

use App\Events\Test\Order\AutoClose;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AutoCloseNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AutoClose  $event
     * @return void
     */
    public function handle(AutoClose $event)
    {
        //
    }
}
