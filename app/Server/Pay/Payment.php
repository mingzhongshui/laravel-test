<?php
/**
 * Created by PhpStorm.
 * User: mingzhongshui
 * Date: 2019/8/22
 * Time: 11:54
 */

namespace App\Server\Pay;

class Payment implements PaymentFactory
{


    const PAY_TYPE_ALI = 'alipay';
    const PAY_TYPE_WECHAT = 'wechat';

    private $payment;

    public function __construct($config, $type)
    {

        switch ($type) {
            case self::PAY_TYPE_ALI:
                $this->payment = new AliPay($config);
                break;
            case self::PAY_TYPE_WECHAT:
                $this->payment = new WechatPay($config);
                break;

        }

    }


    public function formContent()
    {
        return $this->payment->formContent();
    }


    public function verify()
    {
        return $this->payment->verify();
    }



}
