<?php
/**
 * Created by PhpStorm.
 * User: mingzhongshui
 * Date: 2019/8/22
 * Time: 11:50
 */

namespace App\Server\Pay;

interface PaymentFactory
{

    public function formContent();

    public function verify();


}
