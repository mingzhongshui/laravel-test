<?php

namespace App\Console\Commands\Test\Order;

use App\Model\Test\Order\Order;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use mysql_xdevapi\Exception;

class WhileCreateOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:create-while';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'while create order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $i = 15;
        while ($i) {

            try {

                $orderSn = date('ymd') . Redis::connection('queue')->rpop('orderSn');

                $order = new Order;
                $order->orderSn = $orderSn;
                if (!$order->save()) {
                    throw new \Exception('while create order insert error');
                }

                Cache::Store('redis')->put('ORDER_AUTO_CLOSE:' . $order->id,  $order->id, 1);

            } catch (\Exception $e) {
                logger($e->getMessage());
            }

            sleep(2);
            $i--;
        }
    }
}
