<?php

namespace App\Console\Commands\Test\Order;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class AutoCreateOrderSn extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:create-orderSn {--num=100}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $num = $this->option('num');
        $len = strlen($num);
        $init = 1;
        while ($init <= $num)
        {
            try {
                $orderSn = mt_rand(10000, 99999) . substr(time(), -6, 6) . str_pad($init, $len, 0, STR_PAD_LEFT);
                Redis::connection('queue')->lpush('orderSn', $orderSn);

            } catch (\Exception $e) {

                logger($e->getMessage());
                $this->info($e->getMessage());
            }

            $init ++ ;
        }


    }
}
