<?php

namespace App\Console\Commands\Test\RabbmitMQ;

use Illuminate\Console\Command;

use PhpAmqpLib\Connection\AMQPStreamConnection;


class ReceiveTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitMQ:receive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * @throws \ErrorException
     */
    public function handle()
    {

        $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');

        $channel = $connection->channel();

        $channel->queue_declare('test-num', false, true, false, false);

        $this->info("[rabbitMQ start]等待消息。退出按CTRL + C ");

        $channel->basic_qos(null, 1, null);

        $channel->basic_consume('test-num', '', false, false, false, false, function ($msg) {
            $this->info('[rabbitMQ receive]收到消息'. $msg->body );
        });

        while(count($channel->callbacks)) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();
    }
}
