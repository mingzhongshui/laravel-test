<?php
//
// namespace App\Console\Commands\Test\RabbmitMQ\Rpc;
//
// use Illuminate\Console\Command;
// use PhpAmqpLib\Connection\AMQPStreamConnection;
// use PhpAmqpLib\Message\AMQPMessage;
//
// class Client extends Command
// {
//     /**
//      * The name and signature of the console command.
//      *
//      * @var string
//      */
//     protected $signature = 'rabbitMQ:publish-topic {type?} {--msg=topic}';
//
//     /**
//      * The console command description.
//      *
//      * @var string
//      */
//     protected $description = 'rabbitMQ public direct demo';
//
//     private $connection;
//     private $channel;
//     private $callback_queue;
//     private $response;
//     private $corr_id;
//
//     /**
//      * Create a new command instance.
//      *
//      * @return void
//      */
//     public function __construct()
//     {
//         parent::__construct();
//
//         $this->connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
//
//         $this->channel = $this->connection->channel();
//
//         list($this->callback_queue, ,) = $this->channel->queue_declare('', false, false, true, false);
//
//         $this->channel->basic_consume($this->callback_queue, '', false, true, false, false, [
//             $this, 'onResponse'
//         ]);
//
//     }
//
//     /**
//      * Execute the console command.
//      *
//      * @return mixed
//      */
//     public function handle()
//     {
//         try {
//
//             $this->call("30");
//
//         } catch (\Exception $e) {
//
//             $this->info($e->getMessage());
//         }
//
//     }
//
//     public function call($n)
//     {
//         $this->response = null;
//         $this->corr_id = uniqid();
//
//         $msg = new AMQPMessage(
//             (string) $n,
//             [
//                 'correlation_id' => $this->corr_id,
//                 'reply_to'       => $this->callback_queue
//             ]
//         );
//         $this->channel->basic_publish($msg, '', 'rpc_queue');
//
//         while (!$this->response) {
//             $this->channel->wait();
//         }
//
//         return intval($this->response);
//     }
//
//
//     public function onResponse($rep)
//     {
//         if ($rep->get('correlation_id') == $this->corr_id) {
//             $this->response = $rep->body;
//         }
//     }
// }
