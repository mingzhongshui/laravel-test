<?php

namespace App\Console\Commands\Test\RabbmitMQ;

use Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class PublishTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitMQ:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');

        $channel = $connection->channel();

        $channel->queue_declare('test-num', false, true, false, false);

        $num = 1000;

        echo  "[rabbitMQ publish start]发布消息中\n" ;

        while ($num) {
            $msg = new AMQPMessage('hello world! test-----' . $num, ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);

            $channel->basic_publish($msg, '', 'test-num');

            $num--;
        }

        echo  "[rabbitMQ publish end]发布消息结束\n" ;
        //
    }
}
