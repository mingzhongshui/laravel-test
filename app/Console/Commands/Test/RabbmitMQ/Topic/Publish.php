<?php

namespace App\Console\Commands\Test\RabbmitMQ\Topic;

use Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Publish extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitMQ:publish-topic {type?} {--msg=topic}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'rabbitMQ public direct demo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');

            $channel = $connection->channel();

            $channel->exchange_declare('topic_logs', 'topic', false, false, false);

            $routing_key = $this->argument('type') ?: 'info';

            $message = $this->option('msg');

            echo  "[rabbitMQ topic publish start]发布消息中\n" ;

            $msg = new AMQPMessage($message, ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);

            $channel->basic_publish($msg, 'topic_logs', $routing_key);

            echo  "[rabbitMQ topic publish end]发布消息结束\n" ;
            //

        } catch (\Exception $e) {
            $this->info($e->getMessage());
        }

    }
}
