<?php

namespace App\Console\Commands\Test\RabbmitMQ\Exchange;

use Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Publish extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitMQ:publish-exchange';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');

            $channel = $connection->channel();

            // 创建交换机logs
            $channel->exchange_declare('logs', 'fanout', false, false, false);

            // 随机生成队列名
            list($queue_name,,) = $channel->queue_declare("");

            if (!$queue_name) {
                throw new \Exception('rabbitMQ queue create failed');
            }

            // 队列绑定到交换机logs
            $channel->queue_bind($queue_name, 'logs');

            $num = 1000;

            echo  "[rabbitMQ publish start]发布消息中\n" ;

            while ($num) {
                $msg = new AMQPMessage('hello world! test-----' . $num, ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);

                $channel->basic_publish($msg, 'logs');

                $num--;
            }

            echo  "[rabbitMQ publish end]发布消息结束\n" ;
            //

        } catch (\Exception $e) {
            $this->info($e->getMessage());
        }

    }
}
