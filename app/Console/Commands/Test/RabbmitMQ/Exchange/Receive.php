<?php

namespace App\Console\Commands\Test\RabbmitMQ\Exchange;

use Illuminate\Console\Command;

use PhpAmqpLib\Connection\AMQPStreamConnection;


class Receive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitMQ:receive-exchange';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {

        try {
            $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');

            $channel = $connection->channel();

            $channel->exchange_declare('logs', 'fanout', false, false, false);

            $this->info("[rabbitMQ start]等待消息。 退出按CTRL+ C ");


            list($queue_name ,, ) = $channel->queue_declare();
            $channel->queue_bind($queue_name, 'logs');

            // $channel->basic_qos(null, 1, null);

            $channel->basic_consume($queue_name, '', false, false, false, false, function ($msg) {
                $this->info('[rabbitMQ receive]收到消息'. $msg->body );
            });

            while(count($channel->callbacks)) {
                $channel->wait();
            }

            $channel->close();
            $connection->close();

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

    }
}
