<?php

namespace App\Console\Commands\Test\Redis;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class ReceiveTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redis:receive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        $this->info("[*]等待redis消息。退出按CTRL + C ");

        while($num = Redis::connection('queue')->brpop(['test-num'], 0)) {

            $this->info('[x]收到redis消息' . $num[0] . '---------' . $num[1]);

        }
    }
}
