<?php

namespace App\Console\Commands\Test\Redis;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class SendTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redis:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $num = 1000;

        while ($num) {
            Redis::connection('queue')->lpush('test-num', $num);
            $num--;
        }

        $this->info("[x]发送'Hello World！'\n");

    }
}
