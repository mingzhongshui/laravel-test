<?php
/**
 * Created by PhpStorm.
 * User: mingzhongshui
 * Date: 2019/8/22
 * Time: 11:47
 */


namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Model\Test\Order\Order;
use App\Server\Pay\Payment;
use Illuminate\Http\Request;

class OrderController extends Controller
{


    private $request;


    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    public function pay()
    {
        $payType = $this->request->input('payType');


        switch ($payType) {
            case Payment::PAY_TYPE_WECHAT:
                $this->wechat();
                break;
            case Payment::PAY_TYPE_ALI:
                $this->alipay();
                break;
        }

    }

    public function wechat()
    {
        $orderId = $this->request->input('orderId');
        $order = Order::where('id', $orderId);

        $config = config('wechat');


        $config['notify_url'] = 'http://xxx.com/index.php/order/notify';
        $config['async_url'] = 'http://xxx.com/index.php/order/async';
        $config['total_fee'] = $order->price;

        $payment = new Payment($config, Payment::PAY_TYPE_ALI);

        $payment->formContent();

    }

    public function alipay()
    {
        $orderId = $this->request->input('orderId');
        $order = Order::where('id', $orderId);

        $config = config('alipay');

        $config['notify_url'] = 'http://xxx.com/index.php/order/notify';
        $config['async_url'] = 'http://xxx.com/index.php/order/async';
        $config['fee'] = $order->price;

        $payment = new Payment($config, Payment::PAY_TYPE_ALI);

        $payment->formContent();

    }



}