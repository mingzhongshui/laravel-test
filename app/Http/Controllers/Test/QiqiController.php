<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QiqiController extends Controller
{


    public function getAsByTest($name)
    {
        $as = '';
        switch ($name) {
            case 'junebridals.com' :
                $as = 'jbd';
                break;
            case 'dressafford.com' :

                $as = 'dad';
                break;
            case 'dorriswedding.com' :

                $as = 'drw';
                break;
            case 'ucenterdress.com' :

                $as = 'uct';
                break;
        }

        return $as;
    }


    public function test()
    {
        $sites = [];
        foreach ($sites as $site) {
            $as = $this->getAsByTest($site->name);
            $row[] = DB::raw('count(if(sid=' . $site->id . ' && order_from=mobile, 1, Null)) as _' . $as);
            $row[] = DB::raw('count(if(sid=' . $site->id . ' && order_from=site, 1, Null)) as ' . $as);
        }
    }


}
