<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class OrderController extends Controller
{
    //
    private $request;
    public function __construct(Request $request)
    {
        $this->request = $request;

    }

    public function onPutOrder()
    {
        $orderUniqueId = date('ymd') . mt_rand(10000, 99999) . substr(time(), -6, 6);

        Cache::Store('redis')->put('ORDER_AUTO_CLOSE:' . $orderUniqueId, $orderUniqueId, 1);

        dd($orderUniqueId);

    }
}
