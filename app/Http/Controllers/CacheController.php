<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class CacheController extends Controller
{
    //
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function set()
    {

        Redis::set('name', '三岁');
        dd(11);
    }

    public function get()
    {
        $key = $this->request->key;
        $val = Redis::get($key);
        dd($val);
    }

    public function redisPublish()
    {
        Redis::publish('test-channel', $this->request->name);
    }


}
