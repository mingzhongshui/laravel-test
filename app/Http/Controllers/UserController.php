<?php

namespace App\Http\Controllers;

use App\Exports\UsersExport;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;

// use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    
    public function userInfo()
    {

    	return get_class();
    }

    public function lists()
    {

    	$result = DB::table('user')->pluck('name', 'id');
    	var_dump($result);
    	exit;

    	session(['id' => '999']);
    	return session('id');
    	return url()->current();
    	$user_list = [
    		['id' => 1, 'name' => 'xiaoming'],
    		['id' => 2, 'name' => 'xiaohong'],
    	];
    	return view('user_list', ['user_list' => $user_list]);
    }

    public function submit(Request $request)
    {
    	// return 1;
    	return $request->validate([
    		'title' => 'bail|required|max:5',
    	]);
    }

    public function export()
    {
        $result = User::all();
        return \Maatwebsite\Excel\Facades\Excel::download(new UsersExport, 'test.xlsx');
    }

}
